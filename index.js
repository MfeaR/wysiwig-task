const styles = {};

const CLASS_NAME_H1 = "header1-text";
const CLASS_NAME_H2 = "header2-text";
const CLASS_NAME_BOLD = "bold-text";
const CLASS_NAME_ITALIC = "bold-italic";

function setStyles(editor, className, elementName) {
  const element = document.createElement(elementName);
  element.innerText = elementName;
  editor.append(element);

  styles[elementName] = {};
  const clearComputedStyles = getComputedStyle(element);
  const clearStyles = {};
  for (let key in clearComputedStyles) {
    clearStyles[key] = clearComputedStyles[key];
  }

  element.classList.add(className);

  const customStyles = getComputedStyle(element);
  for (let key in customStyles) {
    if (key === "cssText") continue;
    if (customStyles[key] !== clearStyles[key]) {
      styles[elementName][key] = customStyles[key];
    }
  }
  editor.removeChild(element);
}

function applyStyles(domElementName, className, sideEffect = val => {}) {
  const elements = editor.querySelectorAll(domElementName);
  elements.forEach(element => {
    let isExistClassName = false;
    element.classList.forEach(name => {
      if (name === className) isExistClassName = true;
    });
    if (!isExistClassName) {
      element.classList.add(className);
      if (styles[domElementName]) {
        for (let key in styles[domElementName]) {
          element.style[key] = styles[domElementName][key];
        }
      }
    }
    sideEffect(element);
  });
}

const editor = document.querySelector(".edit-area");
setStyles(editor, CLASS_NAME_H1, "h1");
setStyles(editor, CLASS_NAME_H2, "h2");
setStyles(editor, CLASS_NAME_BOLD, "b");
setStyles(editor, CLASS_NAME_ITALIC, "i");

console.log("styles", styles.b);

function registerChangeStyleButton(selector, style, domElementName, className) {
  const button = document.querySelector(selector);
  button.addEventListener("click", () => {
    document.execCommand(style, false, null);
    applyStyles(domElementName, className);
    return false;
  });
}

function registerChangeHeaderStyleButton(selector, header, className) {
  const button = document.querySelector(selector);
  button.addEventListener("click", () => {
    const selection = window.getSelection();
    document.execCommand(
      "insertHTML",
      false,
      `<${header}>${selection}</${header}>`
    );
    applyStyles(header, className, element => {
      element.setAttribute(
        "data-ccp-parastyle",
        `heading ${header.replace("h", "")}`
      ); // To set header in ms office online
    });
    return false;
  });
}

registerChangeStyleButton(".bold", "bold", "b", CLASS_NAME_BOLD);
registerChangeStyleButton(".italic", "italic", "i", CLASS_NAME_ITALIC);

registerChangeHeaderStyleButton(".head-1", "h1", CLASS_NAME_H1);
registerChangeHeaderStyleButton(".head-2", "h2", CLASS_NAME_H2);
